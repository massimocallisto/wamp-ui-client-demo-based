import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Customers from './Customers'
import { BrowserRouter as Router, Switch, Route, Redirect} from 'react-router-dom';
import {Client} from "thruway.js";

var wamp = null;
class App extends Component {




  constructor(props) {
    super(props)

    this.state = {
      atext: [],
      url : 'wss://www.connectanum.com/wamp',
      realm : 'test.unicam.it',
      topic : '/test.unicam.it/test'
    }

    this.handleChangeUrl = this.handleChangeUrl.bind(this);
    this.handleChangeRealm = this.handleChangeRealm.bind(this);
    this.handleChangeTopic = this.handleChangeTopic.bind(this);
    this.handleClick = this.handleClick.bind(this);

  }
  handleChangeUrl(event) {
    this.setState({url :event.target.value});
  } 
  handleChangeRealm(event) {
    this.setState({realm :event.target.value});
  } 
  handleChangeTopic(event) {
    this.setState({topic :event.target.value});
  }
  handleClick(event) {
    event.preventDefault();
    if(wamp != null)
      wamp.close();

    wamp = new Client(this.state.url, this.state.realm);
    wamp.topic(this.state.topic).subscribe((v)=>{
      console.log(v);
      v = v || {};
      this.state.atext.push(v)
      this.setState({atext : this.state.atext});
    });
    
  }

  
  render() {
    return (

      <Router basename={process.env.PUBLIC_URL}>
        <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Simple React App/WAMP</h1>
        </header>
        <div>
          <p>See <a href="https://www.npmjs.com/package/thruway.js" target="new">https://www.npmjs.com/package/thruway.js</a></p>
        <form>
          <p>
            URL:
            <input type="text" name="url" value={this.state.url}  onChange={this.handleChangeUrl} />
          </p>
          <p>
            Realm:
            <input type="text" name="realm" value={this.state.realm} onChange={this.handleChangeRealm}/>
          </p>
          <p>
            Topic:
            <input type="text" name="topic" value={this.state.topic} onChange={this.handleChangeTopic}/>
          </p>
          <input type="button" value="Connect" onClick={this.handleClick}/>
        </form>
        </div>
        <div>
          <p>Wamp messages: <br />
            { this.state.atext.map(element => {
              return JSON.stringify(element);
            })} 
            </p>
        </div>
          {/*<Switch>
                <Route exact path= "/" render={() => (
                  <Redirect to="/customerlist"/>
                )}/>
                 <Route exact path='/customerlist' component={Customers} />
          </Switch>*/}
      </div>
    </Router>
    );
  }
}

export default App;
